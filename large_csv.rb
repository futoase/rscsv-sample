require 'csv'
require 'faker'

CSV.open('large.csv', 'wb') do |csv|
  csv << ['name', 'phone_number', 'email', 'job', 'image_path', 'ipaddress', 'created_at']
  1_000_000.times do |x|
    csv << [
      Faker::Name.name,
      Faker::PhoneNumber.cell_phone,
      Faker::Internet.email,
      Faker::Job.title,
      Faker::File.file_name('image', 'png'),
      Faker::Internet.ip_v4_address,
      Faker::Time.forward(30, :morning)
    ]
  end
end

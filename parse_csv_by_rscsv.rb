require 'rscsv'

record = {}
results = {}

file = File.open('./large.csv')

Rscsv::Reader.each(file.each_line) do |row|
  name = row[0]

  if record[name[0].upcase].nil?
    record[name[0].upcase] = 1
  else
    record[name[0].upcase] += 1
  end
end

record.each do |k, v|
  results[k] = v
end

results.sort.each do |k, v|
  puts "#{k} -> #{v}"
end

file.close

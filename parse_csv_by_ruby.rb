require 'csv'

record = {}
results = {}

CSV.foreach('./large.csv', return_headers: false) do |row|
  name = row[0]

  if record[name[0].upcase].nil?
    record[name[0].upcase] = 1
  else
    record[name[0].upcase] += 1
  end
end

record.each do |k, v|
  results[k] = v
end

results.sort.each do |k, v|
  puts "#{k} -> #{v}"
end
